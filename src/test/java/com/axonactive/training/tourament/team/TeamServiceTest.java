//package com.axonactive.training.tourament.team;
//
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.GregorianCalendar;
//import java.util.List;
//
//import javax.persistence.EntityManager;
//
//import org.hamcrest.core.Is;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import  org.mockito.Mock;
//import org.mockito.invocation.InvocationOnMock;
//
//import static org.junit.Assert.assertThat;
//import static org.mockito.Mockito.*;
//import org.mockito.runners.MockitoJUnitRunner;
//import org.mockito.stubbing.Answer;
//
//import com.axonactive.training.tourament.player.Gender;
//import com.axonactive.training.tourament.player.Player;
//
//@RunWith(MockitoJUnitRunner.class)
//public class TeamServiceTest {
//
//	private Team team ;
//	
//	@InjectMocks
//	TeamService teamService;
//	
//	@Mock
//	EntityManager em;
//	
//	@Before
//	public void testPrepare() {
//		team = new Team("AAVN");
//		team.setId(1);
//	}
//	
//	@Test
//	public void whenAddANewPlayerToEmptyTeam_ThenReturnPlayerID() {
//		
//		Calendar calendar = new GregorianCalendar();
//		calendar.set(2018, 12, 12);
//		
//		Player player = new Player("Nguyen Thi B", "123456789", 7, calendar.getTime(), Gender.MALE);
//		
//		List<Player> players = new ArrayList<>();
//		
//		when(this.em.find(Team.class, 1)).thenReturn(team);
//		
//		doAnswer(new Answer<Player>() {
//			@Override
//			public Player answer(InvocationOnMock invocation) throws Throwable {
//				Player playerResult = (Player)invocation.getArguments()[0];
//				playerResult.setId(1);
//				return playerResult;
//			}
//		}).when(em).persist(player);
//		
//		Integer result  = teamService.add(player, 1);
//		assertThat(result, Is.is(1));
//	}
//	
//	@Test(expected = IllegalArgumentException.class)
//	public void whenAddANullPlayer_ThenThrowIllegalArgumentException() {
//		teamService.add(null,1);
//	}
//	
//	@Test(expected = IllegalArgumentException.class)
//	public void givenAFullMemberTeam_WhenAddANewPlayer_ThenThrowIllegalArgumentException() {
//		
//		Calendar calendar = new GregorianCalendar();
//		calendar.set(2018, 10, 10);
//		Player player = new Player("Nguyen Van A", "123256789", 1, calendar.getTime(), Gender.MALE);
//		
//		for(int i= 1; i <= Team.MAXIMUM_PLAYER_IN_TEAM; i++) {
//			team.getPlayers().add(new Player("Nguyen Van A", "12345678" + String.valueOf(i), 1, calendar.getTime(), Gender.MALE));
//		}
//		
//		when(this.em.find(Team.class, 1)).thenReturn(team);
//		
//		teamService.add(player, team.getId());
//		
//	}
//	
//	@Test
//	public void givenATeam_WhenUpdateTeamName_ThenReturnTeamId() {
//		
//		when(this.em.find(Team.class, 1)).thenReturn(team);
//		
//		when(em.merge(team)).thenReturn(team);
//		
//		teamService.updateTeamName(1, "New Name");
//	}
//	
//	@Test(expected = IllegalArgumentException.class)
//	public void whenUpdateANullTeam_ThenThrowIllegalArgumentException() {
//		
//		when(this.em.find(Team.class, 1)).thenReturn(null);
//		
//		teamService.updateTeamName(1, "New Name");
//		
//	}
//
//	@After
//	public void cleanUp() {
//		team = null;
//	}
//
//}
