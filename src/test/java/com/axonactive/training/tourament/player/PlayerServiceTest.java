///**
// * To test for Player Service APIs
// */
//package com.axonactive.training.tourament.player;
//
//
//import javax.persistence.EntityManager;
//
//import static org.junit.Assert.assertNotNull;
//import static org.mockito.Mockito.*;
//
//import java.util.Date;
//
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.runners.MockitoJUnitRunner;
//
//import com.axonactive.training.tourament.team.Team;
//
///**
// * @author Hutadaa
// *
// */
//@RunWith(MockitoJUnitRunner.class)
//public class PlayerServiceTest {
//		
//		@InjectMocks
//		PlayerService playerService;
//		
//		@Mock EntityManager em;
//		Player player;
//		PlayerDto playerDto;
//		
//		@Mock Team team;
//		
//		@Before
//		public void testPrepare() {
//			player = new Player();
//			playerDto = new PlayerDto("abcd",
//										13,
//										"123456789",
//										new Date(),
//										Gender.MALE,
//										1);
//		}
//				
//		@Test
//		public void whenConvertPlayerDTOToPlayer_ThenReturnNotNullPlayer(){
//			
//			player = null;
//			when(em.find(Team.class, playerDto.getTeamId())).thenReturn(team);
//			player = playerService.convertToEntity(playerDto);
//			
//			assertNotNull(player);
//		}
//		
//		@Test
//		public void whenConvertPlayerToPlayerDTO_ThenReturnNotNullPlayerDTO() {
//			playerDto = null;
//			playerDto = playerService.convertToDto(player);
//			
//			assertNotNull(playerDto);
//		}
//		
//		@After
//		public void cleanUp() {
//			player = new Player();
//		}
//}
