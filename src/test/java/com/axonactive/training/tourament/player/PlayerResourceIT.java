package com.axonactive.training.tourament.player;

import static org.junit.Assert.assertThat;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.core.Response;

import org.hamcrest.core.Is;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.axonactive.common.util.dataset.PropertiesJdbcDataSetManager;
import com.axonactive.common.ws.rs.client.RestClient;


public class PlayerResourceIT {

	private PlayerRestClient client;
	
	private PropertiesJdbcDataSetManager datasetManager;
	
	@BeforeClass
	public static void initDatabaseConfig() throws Exception {
		InputStream dbConfigFile = new FileInputStream(new File("src/test/resources/dbunit-config.properties"));
		System.getProperties().load(dbConfigFile);
	}
	
	@Before
	public void init() throws Exception {
		client = RestClient.createBuilder("http://localhost:8080/tourament-0.0.1-SNAPSHOT/api/").build(PlayerRestClient.class);
		this.datasetManager = new PropertiesJdbcDataSetManager();
		datasetManager.oneSetup("src/test/resources/dataset/player-dataset.xml");
	}
	
//	@Test
//	public void givenATeamWithoutMemberWhenAddNewValidMenberThenHttpCodeIs201() {
//		JsonObjectBuilder memberBuilder = Json.createObjectBuilder();
//		memberBuilder.add("fullName", "Nguyen Van B");
//		memberBuilder.add("socialInsuranceNumber", "1234567890");
//		memberBuilder.add("number", 3);
//		memberBuilder.add("gender", "MALE");
//		memberBuilder.add("birthOfDate", "2000-12-12");
//		memberBuilder.add("team", 1);
//		
//		Response response = this.client.addPlayer(memberBuilder.build());
//		assertThat(response.getStatus(), Is.is(Response.Status.CREATED.getStatusCode()));
//	}
	
	@Test
	public void whenAddAnEmptyPlayer_ThenHttpCodeIs400() {
		
		Response response = this.client.get(0);
		assertThat(response.getStatus(), Is.is(Response.Status.BAD_REQUEST.getStatusCode()));
	}
	
	
	@After
	public void cleanUp() throws Exception {
		this.datasetManager.onTearDown();
	}
}
