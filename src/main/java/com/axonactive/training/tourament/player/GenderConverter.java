package com.axonactive.training.tourament.player;

import javax.persistence.AttributeConverter;

public class GenderConverter implements AttributeConverter<Gender, Integer>{

	@Override
	public Integer convertToDatabaseColumn(Gender gender) {
		return gender.getValue();
	}

	@Override
	public Gender convertToEntityAttribute(Integer data) {
		switch (data){
		case -1:
			return Gender.UNKNKOWN;
		case 0:
			return Gender.FEMALE;
		case 1:
			return Gender.MALE;
		default:
			throw new IllegalStateException("Value is not correct.");
		}
	}

}
