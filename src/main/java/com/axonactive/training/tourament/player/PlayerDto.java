package com.axonactive.training.tourament.player;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "players")
@XmlAccessorType(XmlAccessType.FIELD)
public @Data class PlayerDto implements Serializable {

	private int id;
	
	@NotNull(message = "{Notnull.Player.Name}")
	private String name;
	
	@Min(value = 1, message = "{Possitive.Player.Number}")
	private int number;
	
	@NotNull(message = "{Notnull.Player.SocialInsurranceNumber}")
	private String socialInsurranceNumber;
	
	private Date dateOfBirth;
	
	@NotNull(message = "{Notnull.Player.Gender}")
	private Gender gender;
	
	@NotNull(message = "{Notnull.Player.Team}")
	private Integer teamId;
	
}
