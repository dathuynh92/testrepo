package com.axonactive.training.tourament.player;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.axonactive.training.tourament.team.Team;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@NamedQueries({
	@NamedQuery(name="Player.getBySocialNumber", query="SELECT pl FROM Player pl WHERE pl.socialInsurranceNumber = :social_number"),
	@NamedQuery(name="Player.getAll", query="SELECT a FROM Player a")
}) 



@Entity
@Table(name = "tbl_player")
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement
//@EqualsAndHashCode(of = {"socialInsurranceNumber"})
public @Data class Player implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Integer id;

	
	private static final String getBySocialNumber = "Player.getBySocialNumber";
	private static final String getAll = "Player.getAll";
	
	@Column(name = "name", length = 100, nullable = false)
	String name;

	@Column(name = "social_insurrance_number", length = 9, nullable = false, unique = true)
	String socialInsurranceNumber;

	@Column(name = "number", nullable = false)
	int number;

	@Column(name = "dob", nullable = true)
	@Temporal(TemporalType.DATE)
	Date dateOfBirth;

	@Column(name = "gender", nullable = false)
	@Convert(converter = GenderConverter.class)
	Gender gender;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	Team team;

	public Player(String fullname, String socialInsurranceNumber, int number, Date dob, Gender gender) {
		this.name = fullname;
		this.socialInsurranceNumber = socialInsurranceNumber;
		this.number = number;
		this.dateOfBirth = dob;
		this.gender = gender;
	}
	

}
