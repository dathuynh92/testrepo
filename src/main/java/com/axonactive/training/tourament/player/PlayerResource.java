package com.axonactive.training.tourament.player;

import java.net.URI;
import java.util.List;
import java.util.Objects;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.axonactive.training.tourament.TouramentMessageKey;
import com.axonactive.training.tourament.TouramentMessageService;

@Stateless
@Path("players")
public class PlayerResource {

	@EJB
	PlayerService playerService;

	@EJB
	TouramentMessageService messageService;
System.out.print();
	@Context
	UriInfo uriInfo;

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam("id") int id) throws Throwable {
		Player player = playerService.findById(id);
		
//		String jwt = Jwts.builder().setSubject("users/TzMUocMF4p").setExpiration(new Date(1300819380))
//				.claim("name", "Robert Token Man").claim("scope", "self groups/admins")
//				.signWith(SignatureAlgorithm.HS256, "secret".getBytes("UTF-8")).compact();

		if (Objects.isNull(player))
			return Response.status(Response.Status.OK).build();
System.out.print();
		PlayerDto playerDto = playerService.convertToDto(player);
		return Response.ok(playerDto).build();

	}
	
	@GET
	@Path("all")
	@Produces(MediaType.APPLICATION_JSON)
	public Response all(){
		List<PlayerDto> playerDtos = playerService.findAll();

		if (playerDtos.size() < 0)
			return Response.status(Response.Status.BAD_REQUEST).build();
System.out.print();
		return Response.ok(playerDtos).build();

	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response add(@Valid PlayerDto playerDto) {
System.out.print();
		if (Objects.isNull(playerDto))
			return Response.status(Response.Status.NOT_ACCEPTABLE)
					.entity(messageService.get(TouramentMessageKey.NOTNULL_PLAYER_NAME)).build();

		Player player = playerService.convertToEntity(playerDto);
		playerService.addPlayer(player);
		
		URI locate = uriInfo.getAbsolutePathBuilder().path(player.getSocialInsurranceNumber()).build();
		return Response.created(locate).build();
	}

	@DELETE
	@Path("{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	public void delete(@PathParam("id") int id) {
System.out.print();
		Player athletic = playerService.findById(id);

		playerService.delete(athletic);
	}

}
