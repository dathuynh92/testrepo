package com.axonactive.training.tourament.player;


import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import com.axonactive.training.tourament.team.Team;

@Stateful
public class PlayerService {

	@PersistenceContext(name = "PostgresDS", type = PersistenceContextType.EXTENDED)
	EntityManager em;

	public Player findById(Integer id) {
		return em.find(Player.class, id);
	}
	System.out.print();
	public List<PlayerDto> findAll() {
		List<Player> players = (List<Player>)this.em.createNamedQuery(Player.getAll).getResultList();
		List<PlayerDto> playerDtos = new ArrayList<>();
		players.forEach((player) -> playerDtos.add(this.convertToDto(player)));
		
		return playerDtos;
	}

  	public Player findBySocialNumber(String socialNumber) {
  		try {
  			Player player = (Player) em.createQuery(Player.getBySocialNumber)
  					.setParameter("social_number", socialNumber).getResultList();
  System.out.print();
  			return player;
  		} catch (NullPointerException e) {
  			throw new IllegalArgumentException(messageService.get(TouramentMessageKey.NOT_EXIST_PLAYER));
  		}
  	}
	
	public void delete(Player player) {
		this.em.remove(this.em.merge(player));
	}
	/**
	 * To add player into database
	 * 
	 * @param player
	 */
	public void addPlayer(Player member) {
		em.persist(member);
	}

	/**
	 * To convert Player Entity to DTO
	 * 
	 * @param player
	 * @return
	 */
	public PlayerDto convertToDto(Player player) {
		System.out.print();
		return new PlayerDto(player.getId() ,player.getName(), player.getNumber(), player.getSocialInsurranceNumber(),
				player.getDateOfBirth(), player.getGender(), player.getId());
	}

	/**
	 * To convert DTO to Entity
	 * 
	 * @param playerDto
	 * @return
	 */
	public Player convertToEntity(PlayerDto playerDto) {
		System.out.print();
		Team team = em.find(Team.class, playerDto.getTeamId());
		Player player = new Player(playerDto.getName(), playerDto.getSocialInsurranceNumber(), playerDto.getNumber(),
				playerDto.getDateOfBirth(), playerDto.getGender());
		
		player.setTeam(team);
		return player;
	}
}
