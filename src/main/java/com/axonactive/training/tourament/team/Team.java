package com.axonactive.training.tourament.team;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.ws.rs.Path;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.axonactive.training.tourament.player.Player;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@NamedQueries({
@NamedQuery(name = "Team.getByName", query = "SELECT p FROM Team p WHERE p.name = :name"),
})

@Entity
@Table(name = "tbl_team")
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = {"name"})
@XmlRootElement(name = "teams")
@XmlAccessorType(XmlAccessType.FIELD)
public @Data class Team implements Serializable{

	public static final int MAXIMUM_PLAYER_IN_TEAM = 12;
	public static final int MINIMUM_PLAYER_IN_TEAM = 7;

	public static final String getByName = "Team.getByName";
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "name", length = 100, nullable = false, unique = true)
	private String name;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "team")
	private List<Player> players;

	
	public Team(String name) {
		this.name = name;
		this.players = new ArrayList<>();
	}
	
	

}
