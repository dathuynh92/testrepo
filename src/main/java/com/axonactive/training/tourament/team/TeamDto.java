package com.axonactive.training.tourament.team;


import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class TeamDto implements Serializable {
		
	String name;
}

	
