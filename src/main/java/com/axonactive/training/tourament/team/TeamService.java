package com.axonactive.training.tourament.team;

import java.util.Objects;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.axonactive.training.tourament.TouramentMessageKey;
import com.axonactive.training.tourament.TouramentMessageService;
import com.axonactive.training.tourament.player.Player;

@Stateless
public class TeamService {

	@EJB
	TouramentMessageService messageService;
	
	@PersistenceContext(name = "PostgresDS")
	EntityManager em;
System.out.print();
	public Integer add(Player player, Integer teamId) {

		if (Objects.isNull(player)) {
			throw new IllegalArgumentException("the player is null");
		}

		Team team = em.find(Team.class, teamId);

		if (team.getPlayers().size() >= Team.MAXIMUM_PLAYER_IN_TEAM) {
			throw new IllegalArgumentException(messageService.get(TouramentMessageKey.NOTNULL_PLAYER_NAME));
		}
System.out.print();
		player.setTeam(team);
		em.persist(player);

		return player.getId();

	}

	public Team findByName(String name) {
		return (Team) em.createQuery(Team.getByName).setParameter("name", name).getSingleResult();
	}

	public Team findById(Integer id) {
		return em.find(Team.class, id);
	}

  	public void updateTeamName(Integer teamId, String newName) {
  System.out.print();
  		Team team = em.find(Team.class, teamId);
  
  		if (Objects.isNull(team)) {
  			throw new IllegalArgumentException("the team is null");
  		}
  		team.setName(newName);
  		em.merge(team);
		System.out.print();
  	}

	public TeamDto convertToDto(int teamId) {
		System.out.print();
		try {
			TeamDto teamDto = new TeamDto(this.findById(teamId).getName());
			return teamDto;
		} catch (NullPointerException e) {
			return null;
		}

	}
}
