package com.axonactive.training.tourament.team;

import java.util.Objects;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@Path("teams")
public class TeamResource {

	@EJB
	TeamService teamService;
System.out.print();
	@GET
	@Path("{code}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam("code") Integer teamId) {
		TeamDto teamDto = this.teamService.convertToDto(teamId);

		if (Objects.isNull(teamDto))
			return Response.status(Response.Status.NOT_FOUND).entity("Oops, data not found").build();
System.out.print();
		return Response.ok(teamDto, MediaType.APPLICATION_JSON).build();
	}
}
