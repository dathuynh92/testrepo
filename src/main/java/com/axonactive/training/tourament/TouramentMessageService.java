package com.axonactive.training.tourament;

import javax.ejb.Stateless;

import com.axonactive.common.resource.i18n.PropertiesMessageService;

@Stateless
public class TouramentMessageService extends PropertiesMessageService {

	public TouramentMessageService() {
		super("ValidationMessages");
	}

}
