package com.axonactive.training.tourament;

import javax.ejb.Stateless;

import com.axonactive.common.resource.ResourceKey;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Stateless
public enum TouramentMessageKey implements ResourceKey {

	 NOTNULL_PLAYER_NAME("Notnull.Player.Name"),
	 NOTFOUND_PLAYER("Notfound.Player");
	
	private String key;
	@Override
	public String getName() {
		return this.key;
	}

	
}
